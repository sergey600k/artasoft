export default {
  target: 'static',
  head: {
    title: 'artasoft',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  css: [
    '~assets/scss/base/typography.css',
    '~assets/scss/main.scss'
  ],

  plugins: [],

  components: true,

  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/recaptcha',
    '@nuxtjs/axios'
  ],

  recaptcha: {
    // hideBadge: true,
    siteKey: '6LepVpMfAAAAABsAo9u9qbyO1oshEVQuVehdP0lu', // Site key for requests
    version: 3,
    size: 'compact'
  },

  bootstrapVue: {
    bootstrapCSS: false,
    icons: false
  },

  axios: {
    baseURL: 'https://api-url.com',
    proxyHeaders: false,
    credentials: false
  },

  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['script', 'style', 'font'].includes(type)
      }
    }
  }
}
