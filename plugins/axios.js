import axios from 'axios'

export default ({ store, redirect, app }) => {
  axios.defaults.baseURL = process.env.apiUrl

  axios.defaults.headers.common['X-APP-TYPE'] = 'web_app'
  axios.defaults.headers.common['access-control-allow-origin'] = '*'

  // Request interceptor
  axios.interceptors.request.use((request) => {
    request.baseURL = process.env.apiUrl

    return request
  })
}
