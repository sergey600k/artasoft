const state = () => {
  return {
    genres: [
      { value: 0, label: 'ACTION' },
      { value: 1, label: 'ADVENTURES' },
      { value: 2, label: 'COMEDY' },
      { value: 3, label: 'DRAMA' },
      { value: 4, label: 'HORROR' },
      { value: 5, label: 'WESTERNS' }
    ]
  }
}

const getters = {
  genres: state => state.genres
}

const actions = {
  //  actions
}

export default {
  state,
  getters,
  actions
}
